# Project 1.b

The main solution and code are in the Jupyter Notebook named 'Project_1_b_Solution.ipynb'.

The images used in the notebook are in the MFA_images folder. 

The two images beginning with 'Output' are the two output images from the code, one from SVD and the other obtained from the findHomography matrix of OpenCV.

